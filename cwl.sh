#!/bin/bash
packages="akarimath akariproof akarithesis akaridelim akarioperator akariresource"

showcwl(){
	sed -n -e '/^%% /p' "$1" | cut -c3-
}
gencwl(){
	showcwl "$1.sty" > "$1.cwl"
}
makecwls(){
	for package in $packages; do
		echo Generating cwl for $package
		gencwl "$package"
	done
}
rmcwls(){
	for package in $packages; do
		rm "$package.cwl"
	done
}
installcwls(){
	cwldir=~/AppData/Roaming/texstudio/completion/user
	for package in $packages; do
		echo Installing cwl for $package
		cp "$package.cwl" "$cwldir/$package.cwl"
	done
}

subcommand=$1
case $subcommand in
	"")
		echo USAGE: $0 make/clean/install
		;;
	"make")
		makecwls
		;;
	"clean")
		rmcwls
		;;
	"install")
		makecwls
		installcwls
		;;
esac

